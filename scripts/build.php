<?php
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/translations.php';

function getArgs() {
  global $argv;
  $args = [];
  foreach ($argv as $arg) {
    if (strstr($arg, "=") !== false) {
      list($key, $value) = explode("=", $arg);
      $args[$key] = $value;
    }
  }
  return $args;
}

function translate($string) {
  global $translations;
  return empty($translations[$string]) ? $string : $translations[$string];
}

function getTwig() {
  $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../templates');
  $loader->addPath(__DIR__ . '/../templates', 'kahal_base');

  $twig = new \Twig\Environment($loader, [
  ]);

  $twig->addFilter(new \Twig\TwigFilter('t', 'translate'));

  return $twig;
}

$default_args = [
  'logo_url' => 'https://devapp.kahal.co.il/themes/custom/kahal_base/images/kahal_logo_blue.png',
  'verification_code' => '123abc',
];

$args = array_merge($default_args, getArgs());

$twig = getTwig();
echo $twig->render('@kahal_base/tests/main.html.twig', $args);

