<?php

$translations = [
  'Confirm your email address' => 'יש לאמת את כתובת הדוא"ל',
  'Your confirmation code is below — enter it in your open browser window and we\'ll help you get signed in.' => 'זה קוד האימות שלך, יש להעתיק אותו לתוך חלון האימות בדפדפן',
  'If you didn’t request this email, there’s nothing to worry about — you can safely ignore it.' => 'אם לא ביקשת לקבל הודעה זו, אין בעיה, אפשר להתעלם ממנה.',
  'Create your events with Kahal' => 'צרו אירועים משלכם עם קהל',
];
